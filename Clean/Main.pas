unit Main;

interface
   uses
   System.SysUtils,
   System.Classes,
   Night.Common;
(*---------------------------------------------------------------------------*)
type
TClean = class
private
 FLogs:           TStringArray;
 FFolderCleanName,
 FoutLogFile:     string;
 function InExtAry(const Ext: string; ExtAry: TStringArray):Boolean;
protected
  function IsFolderClean(const Path: string):Boolean;
public
  destructor Destroy; override;
  procedure Execute(const Path: string; ExtAry:  TStringArray);
  property OutlogFile: string read FoutLogFile write FoutLogFile;
  property FolderCleanName: string read FFolderCleanName write FFolderCleanName;
end;

var
    Clean:  TClean;
implementation
   uses
   Night.Consts,
   System.StrUtils,
   System.Masks,
   System.IOUtils,
   System.Types;
(*---------------------------------------------------------------------------*)
destructor TClean.Destroy;
begin
  if DirectoryExists(ExtractFilePath(FoutLogFile)) and ( FLogs.Length > 0 ) then
     FLogs.SaveToFile(FoutLogFile);
  FLogs.Clear;
  inherited Destroy;
end;

function TClean.InExtAry(const Ext: string; ExtAry: TStringArray):Boolean;
var
  E:  string;
begin
  Result:= False;
  if not Ext.IsEmpty then
  begin
    for var S: string in ExtAry do
    begin
      if MatchesMask(Ext,S) then
      begin
        Result:= True;
        Break;
      end;
    end;
  end;
end;

function TClean.IsFolderClean(const Path: string):Boolean;
begin
  Result:= False;
  Var Index:  Byte:= 0;
  for var I: Byte:= Path.Length downto 1 do
      if Path[I] = SC_PathWin then
      begin
        Index:= I;
        Break;
      end;
  var Name: string:= RightStr(Path,Path.Length - Index);
  Result:= Name.ToLower = FFolderCleanName.ToLower;
end;

procedure TClean.Execute(const Path: string; ExtAry:  TStringArray);
var
  DS,
  FS:     Integer;
begin
  DS:= 0;
  FS:= 0;
  if ExtAry.Length > 0 then
  begin
    TDirectory.GetFiles(Path, '*', TSearchOption.soAllDirectories,
    function(const Path: string;const SearchRec: TSearchRec): Boolean
    procedure DoExec(const P:  string);
    const PFormat = '删除%S -> %S  文件大小 = %S 时间 = %S';
    begin
      var S: string:= '';
      with SearchRec do
      begin
        if DeleteFile(P) then
        begin
          S:= Format(PFormat,['成功',P,FormatSize(Size),FormatDateTime('yyyy-mm-dd hh:mm:ss',TimeStamp)]);
          Inc(DS);
        end
        else
        begin
          S:= Format(PFormat,['失败',P,FormatSize(Size),FormatDateTime('yyyy-mm-dd hh:mm:ss',TimeStamp)]);
          Inc(FS);
        end;
      end;
      if not FoutLogFile.IsEmpty then FLogs.Add(S);
      Writeln(S);
    end;
    var
      P:   string;
    begin
      P:= IncludeTrailingPathDelimiter( Path ) + SearchRec.Name;
      if IsFolderClean(Path) then
         DoExec(P)
      else if InExtAry(ExtractFileExt(P),ExtAry) then
         DoExec(P);

      Result:= False;
    end
    );
    var S:  string:= '共处理 = ' + Integer( DS + FS ).ToString +  '  成功 = ' + DS.ToString + '  失败 = ' + FS.ToString;
    Writeln(S);
  end;
end;
end.
