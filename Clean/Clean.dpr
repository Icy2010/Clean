program Clean;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  XSJson,
  Night.Common,
  System.IOUtils,
  Main in 'Main.pas';
(*---------------------------------------------------------------------------*)
begin
  try
    Writeln('清理工程垃圾文件.');
    Writeln('by: icy');
    Writeln('web: icy6.com');
    Writeln('');

    if not FileExists(ParamStr(1)) then
    begin
        Writeln('清理垃圾文件的配置JSON文件不存在或未输入该参数.');
        const Phelp = 'JSON 格式: ' + #13#10#13#10 +
        '{'+#13#10+
        ' "path":"D:\\projects",          //清理目录'+#13#10+
        ' "extary":[".dcu",".o"],        //清理的扩展名'+#13#10+
        ' "csfolder":"backup",          //清理完整的目录名称'+#13#10+
        ' "logpath":"D:\\log"          //日志输出文件'+#13#10+
        '}';
        Writeln(Phelp);

        Halt(0);
    end;

    var JO:  ISuperObject:= SO(TFile.ReadAllText(ParamStr(1)));
    if JO.A['extary'].Length = 0 then
    begin
      Writeln('未输入有效的扩展名,至少输入一个.');
      Halt(0);
    end;

    if not DirectoryExists(JO.S['path']) then
    begin
      Writeln('清理垃圾文件的路径不正确.');
      Halt(0);
    end;

    with TClean.Create do
    begin
      FolderCleanName:= JO.S['csfolder'];
      var  Ary: TStringArray;
      for var I: ShortInt:= 0 to JO.A['extary'].Length-1 do
         Ary.Add(JO.A['extary'].S[I]);

      OutlogFile:= IncludeTrailingPathDelimiter( JO.S['logpath'] ) + formatDatetime('yyyymmddhhmmss',Now) + '.log';
      Execute(JO.S['path'],Ary);
      Ary.Clear;
      Free;
    end;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;

  {$IFDEF DEBUG}
     Readln;
  {$ENDIF}
end.
